package com.infinithink.infinitcalendar.adapter;

import android.content.Context;
import android.util.Log;
import android.util.MonthDisplayHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.infinithink.infinitcalendar.InfiniteCalendarHelper;
import com.infinithink.infinitcalendar.R;
import com.infinithink.infinitcalendar.model.BaseCalendarCell;
import com.infinithink.infinitcalendar.model.SquareCalendarCell;

import java.lang.reflect.Array;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


public class CalendarAdapter extends BaseAdapter {
    private int year;
    private int month;
    private Context context;
    private Calendar calendar;
    private MonthDisplayHelper displayHelper;

    private ArrayList<String> weekDays = new ArrayList<>(Arrays.asList(DateFormatSymbols.getInstance().getShortWeekdays()));


    public CalendarAdapter(Context context, int year, int month, int startDayOfTheWeek) {
        this.context = context;
        initialize(year, month, startDayOfTheWeek);
    }

    public void initialize(int year, int month, int startDayOfTheWeek) {
        this.year = year;
        this.month = month;
        this.calendar = InfiniteCalendarHelper.getLocalizedCalendar(context);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DATE, 1);
        calendar.setFirstDayOfWeek(startDayOfTheWeek);
        displayHelper = new MonthDisplayHelper(year, month, startDayOfTheWeek);
        weekDays.remove(0);
        if (startDayOfTheWeek == Calendar.MONDAY){
            //swap element
            String d = weekDays.get(0);
            weekDays.remove(0);
            weekDays.add(d);
        }
    }

    @Override
    public int getCount() {
        return 49;
    }

    @Override
    public Object getItem(int position) {
        //TODO get day at
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseCalendarCell view;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            if (position < 7) {
                view = (BaseCalendarCell) inflater.inflate(R.layout.calendar_weekday_cell, null);
                TextView day = (TextView) view.getChildAt(0);
                day.setText(weekDays.get(position));
            } else {
                int row = (position-7)/7;
                int col = (position-7)%7;
                view = (BaseCalendarCell) inflater.inflate(R.layout.calendar_cell, null);
                TextView date = (TextView) view.getChildAt(0);
                date.setText(String.valueOf(displayHelper.getDayAt(row, col)));

                if (displayHelper.isWithinCurrentMonth(row, col)) {

                }else {

                }
            }
        } else {
            view = (BaseCalendarCell) convertView;
        }

        return view;
    }
}
