package com.infinithink.infinitcalendar.model;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by wsugiri on 18/10/2016.
 */

public class BaseCalendarCell extends RelativeLayout {
    public BaseCalendarCell(Context context) {
        super(context);
    }

    public BaseCalendarCell(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseCalendarCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BaseCalendarCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
