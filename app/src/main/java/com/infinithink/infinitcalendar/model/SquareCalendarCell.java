package com.infinithink.infinitcalendar.model;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;

/**
 * Created by wsugiri on 18/10/2016.
 */

public class SquareCalendarCell extends BaseCalendarCell {

    public SquareCalendarCell(Context context) {
        super(context);
    }

    public SquareCalendarCell(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareCalendarCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SquareCalendarCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
