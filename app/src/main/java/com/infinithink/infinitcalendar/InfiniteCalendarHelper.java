package com.infinithink.infinitcalendar;

import android.content.Context;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Budi Pratomo on 19-Oct-16.
 */

public class InfiniteCalendarHelper {
    /**
     * Get the current locale
     */
    public static Locale getLocale(Context context){
        return context.getResources().getConfiguration().locale;
    }

    /**
     * @return the localized calendar instance
     */
    public static Calendar getLocalizedCalendar(Context context){
        return Calendar.getInstance(getLocale(context));
    }
}
