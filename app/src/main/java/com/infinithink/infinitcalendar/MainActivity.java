package com.infinithink.infinitcalendar;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.infinithink.infinitcalendar.adapter.CalendarAdapter;
import com.infinithink.infinitcalendar.adapter.CalendarPagerAdapter;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.btn_add)
    Button btnAdd;

    @BindView(R.id.btn_addBegin)
    Button btnAddBegin;

    private ViewPager pager = null;
    private CalendarPagerAdapter pagerAdapter = null;
    private Calendar calendar;
    private int maxMonth;
    private int minMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        pagerAdapter = new CalendarPagerAdapter();
        pager = (ViewPager) findViewById (R.id.view_pager);
        pager.setAdapter (pagerAdapter);

        // Create an initial view to display; must be a subclass of FrameLayout.
        calendar = InfiniteCalendarHelper.getLocalizedCalendar(this);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        minMonth = calendar.get(Calendar.MONTH);
        maxMonth = calendar.get(Calendar.MONTH);

        pagerAdapter.addView (CreateMonthView(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), Calendar.MONDAY), 0);
        pagerAdapter.notifyDataSetChanged();
        AddPrevious();
        AddNext();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNext();
            }
        });

        btnAddBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddPrevious();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == pagerAdapter.getCount()-1) {
                    AddNext();
                }else if (position == 0) {
                    AddPrevious();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private GridView CreateMonthView(int year, int month, int startDayOfTheWeek){
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        GridView newMonth = (GridView) inflater.inflate(R.layout.month_grid_layout, null);

        newMonth.setAdapter(new CalendarAdapter(this, year, month, startDayOfTheWeek));
        return newMonth;
    }

    private void AddNext(){
        calendar.set(Calendar.MONTH, maxMonth);
        calendar.add(Calendar.MONTH, 1);
        maxMonth = calendar.get(Calendar.MONTH);
        pagerAdapter.addView (CreateMonthView(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), Calendar.MONDAY));
        pagerAdapter.notifyDataSetChanged();
    }

    private void AddPrevious(){
        calendar.set(Calendar.MONTH, minMonth);
        calendar.add(Calendar.MONTH, -1);
        minMonth = calendar.get(Calendar.MONTH);
        pagerAdapter.addView (CreateMonthView(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), Calendar.MONDAY), 0);
        pagerAdapter.notifyDataSetChanged();
    }
}
