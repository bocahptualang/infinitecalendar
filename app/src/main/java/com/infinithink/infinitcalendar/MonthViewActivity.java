package com.infinithink.infinitcalendar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.infinithink.infinitcalendar.adapter.CalendarAdapter;

import java.util.Calendar;

public class MonthViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.month_grid_layout);

        GridView gridview = (GridView) findViewById(R.id.gridView);
        Calendar calendar = InfiniteCalendarHelper.getLocalizedCalendar(this);
        gridview.setAdapter(new CalendarAdapter(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), Calendar.MONDAY));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(MonthViewActivity.this, "" + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
